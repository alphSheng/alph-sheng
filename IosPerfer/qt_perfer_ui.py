from PySide2.QtWidgets import QApplication
from PySide2.QtWidgets import QWidget
import time
import tidevice
from tidevice._perf import DataType
import psutil
import threading
import re
import os
import pyqtgraph as pg
from iosPerfer import Ui_Form
from excel_oper import new_perf_file

class Ios_perfer(QWidget):
    def __init__(self):
        super().__init__()
        # loader = QUiLoader()
        # loader.registerCustomWidget(pg.PlotWidget)
        # self.ui = loader.load('iosPerfer.ui')
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self.ui.pushButton_stop.setEnabled(False)
        self.ui.pushButton_start.clicked.connect(self.start_perfer)
        self.ui.pushButton_stop.clicked.connect(self.stop_perfer)
        self.performance_type = {'cpu':DataType.CPU,'memory':DataType.MEMORY,'network':DataType.NETWORK}
        self.flag_new_file = True
        
        ####画布初始化
        self.ui.graphicsView.setTitle('实时监控',color = '008080',size = '12pt')
        self.ui.graphicsView.setBackground('w')
        self.ui.graphicsView.showGrid(x= True,y= True)
        self.curve = self.ui.graphicsView.plot(pen=pg.mkPen('b',width=2))
        self.i = 0
        self.x = []
        self.y = []
        ######workbook初始化
        self.path = 'Iosperf.xlsx'
        self.wb = None
    #引入多线程
    def __start_perfer(self):
        T = threading.Thread(target=self.start_perfer)
        T.start()
    def __stop_perfer(self):
        T = threading.Thread(target=self.stop_perfer)
        T.start()  
    def __write_data_excel(self,value):
        T = threading.Thread(target=self.write_data_excel(value))
    ###写文件
    def write_data_excel(self,value):
        self.wb.write_rows(value,self.flag_new_file)
        self.flag_new_file = False
        
    def draw_dynamic_graph(self,item):
        if item == 'cpu':
            self.ui.graphicsView.setLabel("left","cpu占用率")
            self.ui.graphicsView.setLabel("bottom","时间")
            self.ui.graphicsView.setYRange(min=0, max=100)
        elif item == 'memory':
            self.ui.graphicsView.setLabel("left","内存占用率")
            self.ui.graphicsView.setLabel("bottom","时间")
            self.ui.graphicsView.setYRange(min=0, max=100)
    def update_graph(self,value):
        pattern = re.compile(r'.+?:(.*?)[,\}]')
        list_content = pattern.findall(str(value))
        ###绘制图画，动态添加绘制数据
        self.i += 1
        self.x.append(self.i)
        self.y.append(round(float(list_content[2]),3))
        self.curve.setData(self.x,self.y)
    ###性能参数回调
    def callback(self,_type: tidevice.DataType, value: dict):
        print("R:", _type.value, value)
        self.__write_data_excel(value)
        self.update_graph(value)
    ####点击启动
    def start_perfer(self):
        current_time = time.strftime('%H:%M:%S',time.localtime(time.time()))
        current_time_file = time.strftime('-%H-%M-%S',time.localtime(time.time()))
        perf_item = self.ui.comboBox.currentText()
        sheetname = perf_item+current_time_file
        result_list = self.env_check()
        if result_list[1] == False:
            logs = 'Error: itunes is not running'
            self.oper_log_show(logs)
        elif result_list[0] == False:
            logs = 'Error: device is not online or connected itunes'
            self.oper_log_show(logs)
        else:
            ####页面组件控制
            self.ui.comboBox.setEnabled(False)
            self.ui.pushButton_start.setEnabled(False)
            self.ui.pushButton_stop.setEnabled(True)
            self.ui.lineEdit.setText(' ')
            logs = current_time+' ----启动{}监控'.format(perf_item)
            self.oper_log_show(logs)
            ###画布初始化
            #self.ui.graphicsView.clear()
            self.draw_dynamic_graph(perf_item)
            ##excel文件保存路径(string是元组不可以被改变，先转换为lists)
            list = self.path.split('.')
            self.path = list[0]+current_time_file+'.'+list[1]
            self.wb = new_perf_file(perf_item)
            ####调用性能监控
            perf_item_datatype = self.performance_type[perf_item]
            try:
                self.tidevice = tidevice.Device()
                self.perf = tidevice.Performance(self.tidevice, [perf_item_datatype])
                if self.perf == None:
                    logs = '    环境异常，请检查是否解锁手机、连线、itunes状态'
                    self.oper_log_show(logs)
                else:
                    self.perf.start("com.test.iosclient", callback=self.callback)
            except:
                logs = '    环境异常，请检查是否解锁手机、连线、itunes状态'
                self.oper_log_show(logs)
            else:
                logs = current_time+' 数据统计中'.format(perf_item)
                self.oper_log_show(logs)
    
    def stop_perfer(self):
        self.perf.stop()
        current_time = time.strftime('%H:%M:%S',time.localtime(time.time()))
        logs = current_time+' ----停止监控'
        self.oper_log_show(logs)
        ####文件绘制图表
        self.wb.add_chart()
        self.oper_log_show('    本地文件绘制图表')
        ####文件保存
        self.wb.save_file(self.path)
        self.oper_log_show('    文件保存到本地成功')
        ####刷新控件
        dir = os.getcwd()
        show_path = dir+'\\'+self.path
        self.ui.lineEdit.setText(show_path)
        ###恢复默认值
        self.flag_new_file = True
        self.path = 'Iosperf.xlsx'
        self.ui.pushButton_stop.setEnabled(False)
        self.i = 0
        self.x = []
        self.y = []
        self.wb = None
        time.sleep(2)
        self.ui.pushButton_start.setEnabled(True)
        self.ui.comboBox.setEnabled(True)
        ###刷新qt控件，展示文件路劲
 
    #环境检查
    def env_check(self):
        print('环境check')
        flag_device = False
        flag_itunes = False
        ###判断设备是否连接
        try:
            self.tidevice = tidevice.Device()
            result = self.tidevice.is_connected()
        except AssertionError as e:
            print(e)
            result = False

        if result == True:
            flag_device = True
            print('device is connected itunes')
        else:
            print("device is not online or connected itunes")
        ##判断是否启动itunes
        ps = psutil.pids()
        for pid in ps:
            if psutil.Process(pid).name() == 'iTunes.exe':
                flag_itunes = True
                print('itunes.exe is running')
        return [flag_device,flag_itunes]
    
    def oper_log_show(self,logs):
        self.ui.textBrowser_oper.append(logs)
        self.ui.textBrowser_oper.ensureCursorVisible()


app = QApplication([])
perfer = Ios_perfer()
perfer.show()
app.exec_()