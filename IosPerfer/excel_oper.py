from openpyxl import Workbook
from openpyxl.chart import (
    LineChart,
    Reference,
)
import re

class new_perf_file:
    def __init__(self,sheetname):
        self.wb = Workbook()
        self.ws = self.wb.create_sheet(sheetname,index=0)
        self.sheetname = sheetname
    
    def write_rows(self,value,flag):
        list_content_type = []
        pattern = re.compile(r'.+?:(.*?)[,\}]')
        list_content = pattern.findall(str(value))
        if flag == True:
            pattern_header = re.compile(r'[\{\s](.*?):.*?[,\}]')
            list_header = pattern_header.findall(str(value))
            ###首次写入文件标题
            self.ws.append(list_header)
        ###转换存储格式，excel会默认存为文本，csv会默认存为数值
        if len(list_content) >= 3:
            list_content_type.append(int(list_content[0]))
            list_content_type.append(int(list_content[1]))
            list_content_type.append(round(float(list_content[2]),5))
            if len(list_content) > 3:
                list_content_type.append(round(float(list_content[3]),5))
                list_content_type.append(round(float(list_content[4]),5))
        self.ws.append(list_content_type)

    def add_chart(self):
        min_col = 0
        max_col = 0
        Sum_col_3 = 0
        Sum_col_4 = 0
        chart = LineChart()
        chart.title = self.sheetname
        chart.style = 13
        if self.sheetname == 'cpu':
            chart.y_axis.title = 'CPU Value'
            chart.x_axis.title = 'Times'
            min_col = 3
            max_col = 4
            for i in range(2,self.ws.max_row):
                Sum_col_3 = Sum_col_3 + self.ws.cell(row = i,column = 3).value
                Sum_col_4 = Sum_col_4 + self.ws.cell(row = i,column = 4).value
            Sum_col_3 = Sum_col_3/(float)(self.ws.max_row-1)
            Sum_col_4 = Sum_col_4/(float)(self.ws.max_row-1)
        if self.sheetname == 'memory':
            chart.y_axis.title = 'Memory Value'
            chart.x_axis.title = 'Times'
            min_col = 3
            max_col = 3
            for i in range(2,self.ws.max_row+1):
                Sum_col_3 = Sum_col_3 + self.ws.cell(row = i,column = 3).value
            Sum_col_3 = Sum_col_3/(float)(self.ws.max_row-1)
        
        reference = Reference(self.ws,min_col = min_col,min_row = 1,max_col = max_col,max_row = self.ws.max_row)
        chart.add_data(reference,titles_from_data = True)
        # Style the lines
        for i in  range(0,max_col-min_col+1):
            s1 = chart.series[i]
            s1.smooth = True

        self.ws.add_chart(chart,"G3")
        if Sum_col_4 != 0:
            self.ws.cell(1,7,value='平均值')
            self.ws.cell(1,8,value=Sum_col_4)
        self.ws.cell(2,7,value='平均值')
        self.ws.cell(2,8,value=Sum_col_3)
    
    def save_file(self,filename):
        self.wb.save(filename)
