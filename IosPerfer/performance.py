import time
import tidevice
from tidevice._perf import DataType
import sys
import csv
import re

##监控项
performance_input = 'cpu'
performance_type = {'cpu':DataType.CPU,'memory':DataType.MEMORY,'network':DataType.NETWORK}

type = performance_type[performance_input]
path = performance_input+'.csv'
csv_header_flag = False

t = tidevice.Device()
perf = tidevice.Performance(t, [type])
#  tidevice version <= 0.4.16:
#  perf = tidevice.Performance(t)

def callback(_type: tidevice.DataType, value: dict):
    print("R:", _type.value, value)
    pattern_header = re.compile(r'[\{\s](.*?):.*?[,\}]')
    pattern = re.compile(r'.+?:(.*?)[,\}]')
    list_content = pattern.findall(str(value))
    list_header = pattern_header.findall(str(value))
    global csv_header_flag
    with open(path,'a+',encoding='utf-8',newline='') as f:
        csv_write = csv.writer(f)
        if csv_header_flag == False:
            csv_write.writerow(list_header)
            csv_header_flag = True   
        csv_write.writerow(list_content) 

perf.start("com.test.iosclient", callback=callback)
time.sleep(600)
perf.stop()