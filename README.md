# alphSheng

#### 介绍
https://github.com/alibaba/taobao-iphone-device
基于tidevice完成的ios性能测试工具，支持实时绘制数据图，采集结束后可保存excel文件并绘制图形计算平均值（可采集cpu、memory，后续可扩展其它性能项）

#### 软件架构
软件架构说明
1、主界面窗口通过pyside2实现
2、性能数据调用tidevice接口
3、实施绘图

#### 安装教程

下载py文件，安装依赖的支撑库

#### 使用说明

1.  直接运行qt_perfer_ui.py 主文件

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
